

<?php
// on initialise la variable messages (qui contient la liste des messages) :
$messages = [
    //tableau associatif :
    array(
        "email" => "machin@truc.bd",
        "message" => "le super message de machin"
    ),
    array(
        "email" => "micro@loge.db",
        "message" => "le message de merde de micro"
    )
];

// echo $messages[0]["email"];
// echo $messages; 

?>

    

<div class="container">
    <div class="row">
        <table class="table">
            <thead>
                <tr>
                    <th>Email</th>
                    <th>Message</th>
                </tr>
            </thead>

            <thead>
                <tbody>
                    <tr>
                        <td>partie</td>
                        <td>1 - concatenation</td>
                    </tr>

                    <?php
                    foreach ($messages as $i => $message) {
                        echo "<tr><td>" . $message["email"] . "</td><td>" . $message["message"] . "</td></tr>";
                    }
                    ?>

                    <tr>
                        <td>partie</td>
                        <td>2 - Literals</td>
                    </tr>
                    <?php
                    foreach ($messages as $i => $message) {
                        echo "<tr><td>{$message['email']}</td>
                          <td>${message['message']}</td></tr>";
                    }
                    ?>

                    <tr>
                        <td>partie</td>
                        <td>3 - combinaison html</td>
                    </tr>
                    <!-- partie 3 à utiliser pour le code métier -->
                    <?php foreach ($messages as $i => $message) { ?>
                        <tr>
                            <td><?php echo $message['email']; ?></td>
                            <td><?= $message['message'] ?></td>
                        </tr>
                    <?php } ?>


                    <tr>
                        <td>partie</td>
                        <td>4 - LA syntaxe !!!!</td>
                    </tr>
                    <!-- partie 4 à utiliser pour le code de vue -->
                    <?php foreach ($messages as $i => $message) : ?>
                        <tr>
                            <td><?php echo $message['email']; ?></td>
                            <td><?= $message['message'] ?></td>
                        </tr>
                    <?php endforeach ?>
                    
                </tbody>
            </thead>
        </table>
    </div>
</div>

