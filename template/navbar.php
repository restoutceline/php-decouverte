


<nav class="navbar navbar-expand-lg navbar-light bg-secondary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">PHP Découverte</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <!-- <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./?page=form">Form</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="./?page=messages">Messages</a>
        </li> -->
         
           <?php
           $dir= "content";
           $current_files = array_slice(scandir($dir),2);

          //  print_r($current_files);
          ?>

           <?php foreach ($current_files as $i => $current):?>
            <?php if ($current != "home.php"){?>
            <a class="nav-link" href="./?page=<?=substr($current, 0, -4)?>"><?= substr($current, 0, -4) ?></a>
            <?php } ?>
            <?php endforeach ?>
          
      </ul>
    </div>
  </div>
</nav>
