<!-- <?php   //zone interprétée par php

 echo "hello world";
 echo "<h1>TOTO</h1>";

 ?> -->

 <!DOCTYPE html>
 <html lang="en">
 <head>
     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <title>Formulaire</title>
     <?php include "./template/bootstrap.php"; ?>
 </head>
 <body>

    <?php include "./template/navbar.php"; ?>
    
    <!-- Include the selected content -->
    <?php 
    
   if(isset($_GET["page"])){
       $page = $_GET["page"];
   }else{
       $page = "home";
   }

    // if($page == "home"){
    //     include "./content/home.php";
    // }

    // if($page == "form" ){
    //     include "./content/form.php";
    // }

    // if($page == "messages"){
    //     include "./content/messages.php";
    // }

    //on factorise :
    // include "./content/${page}.php";

     $path = "./content/${page}.php";

   if(is_file($path)){
       include $path;
   }else{
       echo "404";
   }
    ?>

    <?php include "./template/footer.php"; ?>


 </body>
 </html>